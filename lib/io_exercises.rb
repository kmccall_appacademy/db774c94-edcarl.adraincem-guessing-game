# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  target = rand(1..100)
  p target
  input = 0
  answers = []
  until target == input
    print "Guess a number: "
    input = gets.chomp.to_i
    puts input

    if input < 1
      puts "too low"
    elsif input > 99
      puts "too high"
    else
      answers << input
    end
  end

  puts "#{target} is the correct guess!"
  puts "Number of guesses: #{answers.length}"
end

if __FILE__ == $PROGRAM_NAME
  guessing_game
end
